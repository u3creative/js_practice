// date types

//==number==
// var number1 = 35;
// var number2 = 40;
// alert(number1+number2);

//==String==
// alert('My favorite number is ' + number1)

// ==variable==
//can contain letter, number, underscores, dollard signs
// begin with a letter
//case senstive
//camel case "myNumber"
//partial case "MyNumber"
//underscore "my_number"

//==Array==
// var color = ['red','blue','green'];
// alert(color[1]);
//
// var color2 = new Array('red', 'yellow', 'orange');
// color2.push('purple');
// alert(color2.length());
// alert(color2.sort());
// alert(color2.reverse());

//for loop
// for(var i =0; i < 10; i++){
//     console.log('I love the numver' +i);
// }

//==while loop==
// var i = 0
// while(i < 10){
//     console.log(i);
//     i++;
// }

//==forEach loop build to with Array==
// var numbers = [22, 33, 55, 34, 2];
// numbers.forEach(function(numbers){
//     console.log(numbers)
// });

// for(var i = 0; i < numbers.length; i++){
//     console.log(numbers[i]);
// }

//==Conditionals && means and, || means or==
// var var1 = 2;
// var var2 = 8;
//
// if(var1 == var2 && var1 == 2){
//     console.log('this is true');
// } else {
//     console.log('this is false');
// }

//==Switch==
// var fruit ='tree';
// switch (fruit) {
//     case 'apple':
//         alert('apple');
//         break;
//     case 'banana':
//         alert('banana it is');
//         break;
//     default:
//         alert('choose a fruit');
// }


//==Object Literal==
// var person = {
//     firstName:"peter",
//     lastName:'smith',
//     age:"39",
//     children:['kimchi','upup','downdown'], //array
//     address:{ //nesting more object
//         street: "116 Ravine Dr.",
//         city: "Porty Moody",
//         state: "British Columbia",
//     },
//
//     // add function
//     fullName: function(){
//         return this.firstName + ' ' +this.lastName;
//     }
// }
// console.log(person.firstName); // don't have () coz it is not a function
// console.log(person.fullName()); // add () after a function

//=Object Cunstructor
// var apple = new Object();
// apple.color ='red';
// apple.shape ='round';
//
// apple.describe =function(){
//     return ' An apple is the color ' +this.color+ 'and is the shape' +this.shape;
// }
//==Object Cunstructor patton ==
// function Fruit(name, color, shape){
//     this.name = name;
//     this.color= color;
//     this.shape= shape;
//
//     this.describe = function(){
//         return 'An' +this.name+ ' is the color ' +this.color +' and this the shape ' +this.shape;
//     }
// }
//
// var apple = new Fruit('apple', 'red', 'round');
//
// console.log(apple.describe());

//==array of objects
// var user = [
//     {
//         name: 'John Doe',
//         age: 30
//     },
//     {
//         name: 'John Doe2',
//         age: 40
//     },
//     {
//         name: 'John Doe4',
//         age: 39
//     }
// ];
//
// console.log(user[0].name);

//==Events
function doClick(){
    alert('You Clicked!');
}

function changeText1(id){
    id.innerHTML ='You Clicked';
}

function changeText2(){
    var heading = document.getElementById('heading');
    heading.innerHTML ='You Clicked';
}

function showDate(){
    var time = document.getElementById('time');
    time.innerHTML = Date();
}

function showDate2(){
    var time = document.getElementById('time2');
    time.innerHTML = Date();
}

function clearDate(){
    var time = document.getElementById('time2');
    time.innerHTML = '';
}
